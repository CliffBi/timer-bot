from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, declarative_base
from config import DB_STRING

Base = declarative_base()

engine = create_engine(DB_STRING, echo=True)
Session = sessionmaker(bind=engine)
session = Session()


class LocalDbModel(Base):
    __abstract__ = True

    def save(self):
        """
        Save a model instance.

        :return: Model instance
        """
        session.add(self)
        session.commit()

        return self

    def __str__(self):
        """
        Create a human readable version of a class instance.

        :return: self
        """
        obj_id = hex(id(self))
        columns = self.__table__.c.keys()

        values = ', '.join("%s=%r" % (n, getattr(self, n)) for n in columns)
        return '<%s %s(%s)>' % (obj_id, self.__class__.__name__, values)

    @classmethod
    def get_list_all(cls):
        return session.query(cls).all()

    @classmethod
    def get_filtered(cls, *agrs: object) -> object:
        """
        :param agrs:  cls.Column == Value
        :return:
        """
        return session.query(cls).filter(*agrs).all()

    def as_dict(self):
        return {x: getattr(self, x) for x in self.__table__.c.keys()}
