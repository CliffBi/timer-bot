from sqlalchemy import Column, Integer, VARCHAR, DateTime, BigInteger, and_,\
    desc

from main.models.abstract_models import LocalDbModel, session


class TimerRecord(LocalDbModel):
    __tablename__ = 'timer_records'

    id = Column(Integer, primary_key=True)
    user_id = Column(BigInteger, nullable=False)    # telegram uses bigint
    record_name = Column(VARCHAR(32))
    start_record = Column(DateTime, nullable=False)
    end_record = Column(DateTime)
    total_second = Column(Integer)

    @classmethod
    def get_date(cls, start_date, user_id):
        return cls.get_filtered(and_(
            cls.start_record >= start_date,
            cls.user_id == user_id,
            cls.end_record != None
        ))

    @classmethod
    def get_max_id(cls, user_id):
        return session.query(cls).filter(
            cls.user_id == user_id
        ).order_by(
            desc(
                cls.id
            )
        ).first()

    @classmethod
    def check_empty(cls, user_id):
        return cls.get_filtered(cls.user_id == user_id)

    def __repr__(self):
        return f'{self.record_name}, {self.start_record}, {self.end_record},' \
               f' {self.total_second}'

    @classmethod
    def get_current_time_record(cls, user_id):
        return session.query(cls).filter(
            cls.user_id == user_id, cls.end_record == None
        ).first()

    @classmethod
    def get_current_name_record(cls, user_id):
        return session.query(cls).filter(
            cls.user_id == user_id, cls.record_name == None
        ).first()
