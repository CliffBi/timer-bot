from main.models.timer_model import TimerRecord


def create_new_record(user_id: int, start_record) -> None:
    """creating a new record with user id and recording start time"""
    TimerRecord(user_id=user_id, start_record=start_record).save()


def add_end_time(user_id: int, last_record) -> TimerRecord:
    """updating the previously created entry by adding the end time"""
    select_last_user_record = TimerRecord.get_max_id(user_id)
    differ = last_record - select_last_user_record.start_record
    select_last_user_record.end_record = last_record
    select_last_user_record.total_second = differ.total_seconds()
    TimerRecord.save(select_last_user_record)
    select_last_user_record.save()
    return differ.total_seconds()


def add_record_name(user_id: int, record_name) -> None:
    """updating the previously created entry by adding the name for record"""
    select_last_user_record = TimerRecord.get_max_id(user_id)
    select_last_user_record.record_name = record_name
    select_last_user_record.save()


def daily_history_view(user_id: int, query_dict) -> TimerRecord:
    """sends a query to the database to display records for a certain period
    of time"""
    records = TimerRecord.get_date(
        query_dict,
        user_id)
    return records


def check_last_record(user_id: int) -> bool:
    """checking the last created entry in the database"""
    return bool(TimerRecord.get_current_time_record(user_id))


def check_last_name_record(user_id: int) -> bool:
    return bool(TimerRecord.get_current_name_record(user_id))
