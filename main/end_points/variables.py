TOTAL_FOR_DAY = 'Total for day'
TOTAL_FOR_WEEK = 'Total for week'
TOTAL_FOR_MONTH = 'Total for month'

TODAY_RECORDS = "Today's records"
WEEK_RECORDS = "Week's records"
MONTH_RECORDS = "Month's records"

DAY = 'day'
WEEK = 'week'
MONTH = 'month'

START_TIMER = 'Start timer'
STOP_TIMER = 'Stop timer'
REPORTS = 'Reports'
TOTAL_TIME = 'Total time'

NO_NAME = 'No name'

TEXT = 'text'
DATA = 'data'
