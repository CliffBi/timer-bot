import datetime
import time
from datetime import date
from functools import reduce

from telebot import types

from main.db_layers import check_last_record
from main.end_points.buttons import START_MENU_BUTTONS
from main.end_points.engine import bot
from main.end_points.variables import DAY, WEEK, MONTH, TEXT, START_TIMER, \
    STOP_TIMER, TOTAL_TIME


def send_msg_with_call_data(call, **param) -> None:
    bot.send_message(**param)
    bot.answer_callback_query(callback_query_id=call.id)


def creating_buttons(
        chat_id: int, buttons: list[dict[str, str]]
) -> types.InlineKeyboardMarkup:

    list_with_buttons = buttons.copy()

    last_record_flag = check_last_record(chat_id)

    button = [
        types.InlineKeyboardButton(STOP_TIMER, callback_data=STOP_TIMER)
        if last_record_flag else
        types.InlineKeyboardButton(START_TIMER, callback_data=START_TIMER)
    ]
    list_with_buttons.insert(0, button)
    return types.InlineKeyboardMarkup(list_with_buttons)


def time_calculation(time_period: str) -> datetime:
    date_now = date.today()

    if time_period == DAY:
        return date_now
    elif time_period == WEEK:
        return date_now - datetime.timedelta(date_now.weekday())
    elif time_period == MONTH:
        return date_now - datetime.timedelta(date_now.day + 1)


def time_formatting(record) -> str:
    return f'{record.day}/{record.month}/{record.year},' \
           f' {record.hour}:{record.minute}'


def convert_sec_to_time(total_second: int) -> str:
    total_time = time.gmtime(total_second)
    return time.strftime("%H:%M:%S", total_time)


def get_total_second(reports) -> int:
    return reduce((lambda seconds, total: total + seconds), reports)


def get_total_time(call, user_id: int, reports) -> None:
    total_time = 'Records not found' if not reports else convert_sec_to_time(
        get_total_second((report.total_second for report in reports))
    )
    send_msg_with_call_data(
        call,
        chat_id=user_id,
        text=f'{TOTAL_TIME}: {total_time}',
    )
    send_msg_with_call_data(
        call,
        chat_id=user_id,
        text="Select an action:",
        reply_markup=creating_buttons(user_id, START_MENU_BUTTONS)
    )


def send_history_msg(call, user_id: int, report) -> None:
    send_msg_with_call_data(
        call,
        chat_id=user_id,
        text=f"Title: {report.record_name}\n"
             f"Start recording: {time_formatting(report.start_record)}\n"
             f"End of record: {time_formatting(report.end_record)}\n"
             f"Difference in time: {convert_sec_to_time(report.total_second)}",
    )


def get_history_per_time(call, user_id: int, reports) -> None:
    """returns the history of records for a specified time."""
    [send_history_msg(call, user_id, report) for report in reports]
    get_total_time(call, user_id, reports)
