from datetime import datetime

from main.db_layers import create_new_record, add_end_time, add_record_name, \
    check_last_record, \
    check_last_name_record, daily_history_view
from main.end_points.buttons import TOTAL_TIME_MENU_BUTTONS, \
    REPORTS_MENU_BUTTONS, \
    START_MENU_BUTTONS, NO_NAME_BUTTON
from main.end_points.common import get_history_per_time, time_calculation, \
    get_total_time, send_msg_with_call_data, convert_sec_to_time, \
    creating_buttons
from main.end_points.engine import bot
from main.end_points.variables import TOTAL_FOR_DAY, TOTAL_FOR_WEEK, \
    TOTAL_FOR_MONTH, TODAY_RECORDS, WEEK_RECORDS, MONTH_RECORDS, DAY, WEEK, \
    MONTH, TOTAL_TIME, NO_NAME, STOP_TIMER, START_TIMER, TEXT, REPORTS

TIME_SPAN = {
    TODAY_RECORDS: DAY,
    WEEK_RECORDS: WEEK,
    MONTH_RECORDS: MONTH,
}

TOTAL_TIME_SPAN = {
    TOTAL_FOR_DAY: DAY,
    TOTAL_FOR_WEEK: WEEK,
    TOTAL_FOR_MONTH: MONTH
}


@bot.message_handler(commands=['start', 'help'])
def starter(message) -> None:
    """user welcome message"""
    user_id = message.chat.id

    if message.text == '/start':
        user_name = message.from_user.first_name
        bot.send_message(
            chat_id=user_id,
            text=f"Welcome {user_name}! You can start recording a segment of "
                 f"time or view existing segments.",
            reply_markup=creating_buttons(user_id, START_MENU_BUTTONS)
        )

    elif message.text == '/help':
        bot.send_message(chat_id=user_id, text="Instruction")


@bot.callback_query_handler(func=lambda call: call.data == START_TIMER)
def start_timer(call) -> None:
    """starts a timer and records the start time"""
    user_id = call.message.chat.id

    last_record_flag = check_last_record(user_id)

    if not last_record_flag:
        create_new_record(user_id, datetime.now())

        send_msg_with_call_data(
            call,
            chat_id=user_id,
            text="Timer started",
            reply_markup=creating_buttons(user_id, START_MENU_BUTTONS)
        )
    else:
        send_msg_with_call_data(call, chat_id=user_id, text="Can't start timer")


@bot.callback_query_handler(func=lambda call: call.data == STOP_TIMER)
def stop_timer(call) -> None:
    """stops the timer and records the end time"""
    user_id = call.message.chat.id

    last_record_flag = check_last_record(user_id)

    if last_record_flag:
        seconds = add_end_time(user_id, datetime.now())

        send_msg_with_call_data(
            call,
            chat_id=user_id,
            text=f"The timer is stopped, the total length of time is "
                   f"{convert_sec_to_time(seconds)}. Enter a name for the "
                   f"entry.",
            reply_markup=creating_buttons(user_id, NO_NAME_BUTTON)
        )
    else:
        send_msg_with_call_data(call, chat_id=user_id, text="Can't stop timer")


@bot.callback_query_handler(func=lambda call: call.data == REPORTS)
def get_report_buttons(call) -> None:
    """displays select buttons for displaying the history of records"""
    user_id = call.message.chat.id

    send_msg_with_call_data(
        call,
        chat_id=user_id,
        text="For what period of time do you want to see history of records?",
        reply_markup=creating_buttons(user_id, REPORTS_MENU_BUTTONS)
    )


@bot.callback_query_handler(func=lambda call: call.data == TOTAL_TIME)
def get_total_time_report_buttons(call) -> None:
    """displays select buttons for displaying the history of records"""
    user_id = call.message.chat.id

    send_msg_with_call_data(
        call,
        chat_id=user_id,
        text="For what period of time do you want to see the total time?",
        reply_markup=creating_buttons(user_id, TOTAL_TIME_MENU_BUTTONS)
    )


@bot.callback_query_handler(func=lambda call: call.data == NO_NAME)
def create_entry_without_name(call) -> None:
    """handles character keyboard input"""
    user_id = call.message.chat.id

    examination = check_last_record(user_id)
    examination_for_name = check_last_name_record(user_id)

    if not examination and examination_for_name:
        """naming the last entry"""
        add_record_name(user_id, NO_NAME)

        send_msg_with_call_data(
            call,
            chat_id=user_id,
            text="You have successfully created an entry. Want to start a new"
                 "recording?",
            reply_markup=creating_buttons(user_id, START_MENU_BUTTONS)
        )


@bot.callback_query_handler(func=lambda call: call.data in TIME_SPAN)
def get_reports(call) -> None:
    """returns all records for the day"""
    user_id = call.message.chat.id

    reports_from_database = daily_history_view(
        user_id,
        time_calculation(TIME_SPAN[call.data])
    )

    get_history_per_time(call, user_id, reports_from_database)


@bot.callback_query_handler(
    func=lambda call: call.data in TOTAL_TIME_SPAN
)
def get_total_timer_reports(call) -> None:
    """returns all records for the day"""
    user_id = call.message.chat.id

    reports_from_database = daily_history_view(
        user_id,
        time_calculation(TOTAL_TIME_SPAN[call.data])
    )

    get_total_time(call, user_id, reports_from_database)


@bot.message_handler(content_types=TEXT)
def add_name_to_entry(message) -> None:
    """handles character keyboard input"""
    user_id = message.chat.id

    examination = check_last_record(user_id)
    examination_for_name = check_last_name_record(user_id)

    if not examination and examination_for_name:
        """naming the last entry"""
        add_record_name(user_id, message.text)

        bot.send_message(
            chat_id=user_id,
            text="You have successfully given the entry a name. Want to"
                 "start recording?",
            reply_markup=creating_buttons(message.chat.id, START_MENU_BUTTONS)
        )
    else:
        bot.send_message(
            chat_id=user_id, text="Can't enter a title for the post right now"
            )
