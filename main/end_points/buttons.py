from telebot import types

from main.end_points.variables import TOTAL_FOR_DAY, TOTAL_FOR_WEEK, \
    TOTAL_FOR_MONTH, TODAY_RECORDS, WEEK_RECORDS, MONTH_RECORDS, REPORTS, \
    TOTAL_TIME, NO_NAME

START_MENU_BUTTONS = [
    [types.InlineKeyboardButton(REPORTS, callback_data=REPORTS)],
    [types.InlineKeyboardButton(TOTAL_TIME, callback_data=TOTAL_TIME)]
]

REPORTS_MENU_BUTTONS = [
    [types.InlineKeyboardButton(TODAY_RECORDS, callback_data=TODAY_RECORDS)],
    [types.InlineKeyboardButton(WEEK_RECORDS, callback_data=WEEK_RECORDS)],
    [types.InlineKeyboardButton(MONTH_RECORDS, callback_data=MONTH_RECORDS)]
]

TOTAL_TIME_MENU_BUTTONS = [
    [types.InlineKeyboardButton(TOTAL_FOR_DAY, callback_data=TOTAL_FOR_DAY)],
    [types.InlineKeyboardButton(TOTAL_FOR_WEEK, callback_data=TOTAL_FOR_WEEK)],
    [types.InlineKeyboardButton(TOTAL_FOR_MONTH, callback_data=TOTAL_FOR_MONTH)]
]

NO_NAME_BUTTON = [
    [types.InlineKeyboardButton(NO_NAME, callback_data=NO_NAME)]
]
