1. This bot is designed for direct interaction with the user. The main task is the ability to record time manually
  and assign names to each segment of time, the ability to view the amount of time spent (hh.mm) and the date
  creating a time segment, the ability to delete saved time segments.

2. Bot features.
2.1 Start and stop time manually.
2.2 Naming a recorded time.
2.3 Viewing the history of records of time intervals (Name, recorded time in hh.mm, date of creation of the record).
