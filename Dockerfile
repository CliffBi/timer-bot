FROM python:3.10

WORKDIR /TimerBot

COPY . .

RUN pip install -r requirements.txt

CMD python3 run.py