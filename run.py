from main.end_points.timer import bot
from main.models.abstract_models import engine, Base

Base.metadata.create_all(engine)


bot.polling(none_stop=True, interval=0)
