from os import getenv


TOKEN = getenv('TB_TOKEN')
DB_USER = getenv('DB_USER')
DB_PASSWD = getenv('DB_PASSWD')
DB_HOST = getenv('DB_HOST')
DB_NAME = getenv('DB_NAME')
DB_STRING = f"postgresql://{DB_USER}:{DB_PASSWD}@{DB_HOST}/{DB_NAME}"
